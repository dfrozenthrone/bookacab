package com.example.bookacab;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.bookacab.utils.SessionManager;

public class DashboardActivity extends android.support.v4.app.Fragment implements OnClickListener {

	private Button btnBook, btnBooking, btnSetting, btnLogout;
	private View rootView;
	
	SessionManager session;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		rootView = inflater
				.inflate(R.layout.dashboard_layout, container, false);
		intialize();
		
		session 		= new SessionManager(getActivity());
		
		btnBook.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				/*LoginActivity dFrag = new LoginActivity();
				android.support.v4.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.replace(R.id.fragmentDashboard, dFrag);
				transaction.addToBackStack(null);
				transaction.commit();*/
				
				Toast.makeText(getActivity(), "Book button is presed", 3000).show();
				
				
			}
		});
		
		
		btnLogout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				session.logoutUser();
				
				DashboardActivity dash = new DashboardActivity();
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				transaction.remove(dash).commit();
				
			}
		});
		
		
		
		return rootView;
	}

	private void intialize() {
		btnBook = (Button) rootView.findViewById(R.id.btnDashBook);
		btnBooking = (Button) rootView.findViewById(R.id.btnDashBooking);
		btnSetting = (Button) rootView.findViewById(R.id.btnDashSetting);
		btnLogout = (Button) rootView.findViewById(R.id.btnDashLogout);
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getActivity().getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub

	}
}

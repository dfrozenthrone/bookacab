package com.example.bookacab;

import java.io.IOException;
import java.util.List;

import org.apache.http.util.LangUtils;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.bookacab.utils.GPSTracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

@SuppressLint("NewApi")
public class TrackActivity extends Activity implements OnMapClickListener,
		OnMarkerDragListener {

	private GoogleMap googleMap;
	private GPSTracker gps;
	private LatLng location;
	private double LatitudeD, LongitudeD;
	FragmentManager manager;
	private List<Address> addressList;
	MarkerOptions marker;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.track_layout);

		ActionBar ab = getActionBar();
		ab.setHomeButtonEnabled(true);
		try {
			initilizeMap();

			googleMap.setMapType(googleMap.MAP_TYPE_NORMAL);
			// Showing / hiding your current location
			googleMap.setMyLocationEnabled(true);

			// Enable / Disable zooming controls
			googleMap.getUiSettings().setZoomControlsEnabled(true);

			// Enable / Disable my location button
			googleMap.getUiSettings().setMyLocationButtonEnabled(true);

			// Enable / Disable Compass icon
			googleMap.getUiSettings().setCompassEnabled(true);

			// Enable / Disable Rotate gesture
			googleMap.getUiSettings().setRotateGesturesEnabled(true);

			// Enable / Disable zooming functionality
			googleMap.getUiSettings().setZoomGesturesEnabled(true);

			// Adding a marker
			marker = new MarkerOptions().position(
					new LatLng(27.72517, 85.325861)).title("Hello Maps ");

			marker.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.cab_ico));

			marker.draggable(true);

			googleMap.addMarker(marker);

			// assign current location
			location = marker.getPosition();

			googleMap.setOnMarkerDragListener(this);
			CameraPosition cameraPosition = new CameraPosition.Builder()
					.target(new LatLng(27.72517, 85.325861)).zoom(15).build();

			googleMap.animateCamera(CameraUpdateFactory
					.newCameraPosition(cameraPosition));

		} catch (Exception e) {
			// TODO: handle exception
			Toast.makeText(getApplicationContext(), e.getMessage(), 3000)
					.show();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		initilizeMap();
	}

	/**
	 * function to load map If map is not created it will create it for you
	 * */
	@SuppressLint("NewApi")
	private void initilizeMap() {
		if (googleMap == null) {
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();

			// check if map is created successfully or not
			if (googleMap == null) {
				Toast.makeText(getApplicationContext(),
						"Sorry! unable to create maps", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}

	private void updateMarker() {

	}

	@Override
	public void onMapClick(LatLng latLong) {
		// TODO Auto-generated method stub

		if (latLong == location) {
			Toast.makeText(TrackActivity.this, "Marker Clicked",
					Toast.LENGTH_LONG).show();
		}
	}

	private double[] giveLatLong() {
		location = marker.getPosition();

		double[] locDoubleList = null;

		locDoubleList[0] = location.latitude;
		locDoubleList[1] = location.longitude;

		return locDoubleList;

	}

	private List<Address> getLocationFrom(double lat, double longitude,
			int maxResults) {
		Geocoder geoCoder = new Geocoder(TrackActivity.this);
		List<Address> address = null;
		try {
			address = geoCoder.getFromLocation(lat, longitude, maxResults);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Toast.makeText(TrackActivity.this,
					e.getLocalizedMessage().toString(), Toast.LENGTH_LONG)
					.show();
		}

		return address;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.track_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.menuTrackDone) {

			Log.d("Location", "lat" + Double.toString(location.latitude)
					+ "Long:" + Double.toString(location.longitude));

			addressList = getLocationFrom(location.latitude,
					location.longitude, 1);
			Address addressLists = addressList.get(0);
			String addressOne = addressLists.getAddressLine(0).toString()+addressLists.getLocality();
			Toast.makeText(TrackActivity.this, "Address :" + addressOne, 500000)
					.show();

		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onMarkerDrag(Marker argMarker) {
		// TODO Auto-generated method stub
		location = argMarker.getPosition();
		Log.d("Location", "lat" + Double.toString(location.latitude) + "Long:"
				+ Double.toString(location.longitude));
	}

	@Override
	public void onMarkerDragEnd(Marker argMarker) {
		// TODO Auto-generated method stub
		location = argMarker.getPosition();

		Toast.makeText(
				TrackActivity.this,
				"lat" + Double.toString(location.latitude) + "Long:"
						+ Double.toString(location.longitude), 500000).show();
		Log.d("Location", "lat" + Double.toString(location.latitude) + "Long:"
				+ Double.toString(location.longitude));

	}

	@Override
	public void onMarkerDragStart(Marker arg0) {
		// TODO Auto-generated method stub
	}

}

package com.example.bookacab;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class GetaQuoteFormFragment extends Fragment {

	View rootView;
	Button btnContinue;
	private EditText etPickDate, etPickTime, etPickLocation;
	private Integer year, month, day;
	StringBuilder stringDate, stringTime;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_get_quote_form,
				container, false);

		intializeComp();

		etPickDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				DialogFragment dialogFragment = new SelectDateFragment();
				dialogFragment.show(getFragmentManager(), "Date Picker");
			}
		});

		etPickTime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				DialogFragment dialogFragment = new SelectTimeFragment();
				dialogFragment.show(getFragmentManager(), "Date Picker");
			}
		});

		etPickLocation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DialogFragment dialogFragment = new MapDialogFragment();
				dialogFragment.show(getFragmentManager(), "Map Dialog");
			}
		});

		btnContinue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				BookingDetailFragment dFrag = new BookingDetailFragment();
				android.support.v4.app.FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				// Replace whatever is in the fragment_container view with this
				// fragment,
				// and add the transaction to the back stack
				transaction.add(R.id.fragmentGetQuoteForm, dFrag);
				transaction.addToBackStack(null);
				transaction.commit();
			}
		});

		return rootView;
	}

	private void intializeComp() {
		btnContinue = (Button) rootView.findViewById(R.id.btnGetQuoteContinue);
		etPickDate = (EditText) rootView
				.findViewById(R.id.etGetQuoteFrmPickDate);
		etPickTime = (EditText) rootView
				.findViewById(R.id.etGetQuoteFrmPickTime);

		etPickLocation = (EditText) rootView.findViewById(R.id.etPickLocForm);

	}

	public void setDate(View view) {
		DialogFragment dialogFragment = new SelectDateFragment();
		dialogFragment.show(getFragmentManager(), "Date Picker");

	}

	public class SelectDateFragment extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final Calendar calendar = Calendar.getInstance();
			int yy = calendar.get(Calendar.YEAR);
			int mm = calendar.get(Calendar.MONTH);
			int dd = calendar.get(Calendar.DAY_OF_MONTH);
			return new DatePickerDialog(getActivity(), this, yy, mm, dd);
		}

		public void onDateSet(DatePicker view, int yy, int mm, int dd) {
			showDate(yy, mm + 1, dd);
		}

		private void showDate(int year, int month, int day) {

			stringDate = new StringBuilder().append(day).append("/")
					.append(month).append("/").append(year);
			etPickDate.setText(stringDate.toString());
		}

	}

	private class SelectTimeFragment extends DialogFragment implements
			TimePickerDialog.OnTimeSetListener {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final Calendar calendar = Calendar.getInstance();
			int hours = calendar.get(Calendar.HOUR_OF_DAY);
			int minute = calendar.get(Calendar.MINUTE);
			return new TimePickerDialog(getActivity(), this, hours, minute,
					false);

		}

		private void showDate(int hours, int minute) {

			stringTime = new StringBuilder().append(hours).append("hrs")
					.append(minute);
			etPickTime.setText(stringTime.toString());
		}

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// TODO Auto-generated method stub
			showDate(hourOfDay, minute);
		}
	}

	private class MapDialogFragment extends DialogFragment implements
			OnMarkerDragListener {

		GoogleMap googleMap;
		private LatLng location;
		private List<Address> addressList;
		MarkerOptions marker;
		SupportMapFragment supportMapFrag;
		EditText etLocation ;

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {

			// custom dialog
			final Dialog dialog = new Dialog(getActivity());
			dialog.setContentView(R.layout.fragment_dialog_map);
			dialog.setTitle("Set Address");

			// set the custom dialog components - text, image and button
			TextView text = (TextView) dialog
					.findViewById(R.id.tvDialogFragAddress);
			etLocation  = (EditText) dialog.findViewById(R.id.etDialogMapAdd);
			
			supportMapFrag = ((SupportMapFragment) getFragmentManager()
					.findFragmentById(R.id.mapDialogFragment));

			if (supportMapFrag == null) {
				/*
				 * FragmentManager fragmentManager = getFragmentManager();
				 * FragmentTransaction fragmentTransaction = fragmentManager
				 * .beginTransaction(); supportMapFrag =
				 * SupportMapFragment.newInstance();
				 * fragmentTransaction.replace(R.id.mapDialogFragment,
				 * supportMapFrag).commit();
				 */

				Toast.makeText(getActivity(), "Map cant be created", 5000)
						.show();
			}

			if (supportMapFrag != null) {
				googleMap = supportMapFrag.getMap();
				setupMap();
			}

			Button dialogButton = (Button) dialog
					.findViewById(R.id.btnDialogFragMap);
			// if button is clicked, close the custom dialog
			dialogButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {

					/*addressList = getLocationFrom(location.latitude,
							location.longitude, 1);
					Address addressLists = addressList.get(0);
					String addressOne = addressLists.getAddressLine(0)
							.toString() + addressLists.getLocality();
					Toast.makeText(getActivity(), "Address :" + addressOne,
							500000).show();*/
					
					 // Getting user input location
	                String location = etLocation.getText().toString();
	 
	                if(location!=null && !location.equals("")){
	                    new GeocoderTask().execute(location);
	                }
				}
			});

			return dialog;

		}

		private void setupMap() {
			googleMap.setMapType(googleMap.MAP_TYPE_NORMAL);
			// Showing / hiding your current location
			googleMap.setMyLocationEnabled(true);

			// Enable / Disable zooming controls
			googleMap.getUiSettings().setZoomControlsEnabled(true);

			// Enable / Disable my location button
			googleMap.getUiSettings().setMyLocationButtonEnabled(true);

			// Enable / Disable Compass icon
			googleMap.getUiSettings().setCompassEnabled(true);

			// Enable / Disable Rotate gesture
			googleMap.getUiSettings().setRotateGesturesEnabled(true);

			// Enable / Disable zooming functionality
			googleMap.getUiSettings().setZoomGesturesEnabled(true);

		}

		public void onMapReady(GoogleMap map) {

		}

		@Override
		public void onMarkerDrag(Marker arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onMarkerDragEnd(Marker argMarker) {
			// TODO Auto-generated method stub

			location = argMarker.getPosition();

			Toast.makeText(
					getActivity(),
					"lat" + Double.toString(location.latitude) + "Long:"
							+ Double.toString(location.longitude), 500000)
					.show();
		}

		@Override
		public void onMarkerDragStart(Marker arg0) {
			// TODO Auto-generated method stub

		}

		private List<Address> getLocationFrom(double lat, double longitude,
				int maxResults) {
			Geocoder geoCoder = new Geocoder(getActivity());
			List<Address> address = null;
			try {
				address = geoCoder.getFromLocation(lat, longitude, maxResults);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Toast.makeText(getActivity(),
						e.getLocalizedMessage().toString(), Toast.LENGTH_LONG)
						.show();
			}

			return address;
		}

		public void onMapLoaded() {
			// TODO Auto-generated method stub
			// Adding a marker
			marker = new MarkerOptions().position(
					new LatLng(27.72517, 85.325861)).title("Hello Maps ");

			marker.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.cab_ico));

			marker.draggable(true);

			location = marker.getPosition();

			googleMap.addMarker(marker);

			CameraPosition cameraPosition = new CameraPosition.Builder()
					.target(new LatLng(27.72517, 85.325861)).zoom(15).build();

			googleMap.animateCamera(CameraUpdateFactory
					.newCameraPosition(cameraPosition));
		}

		private class GeocoderTask extends
				AsyncTask<String, Void, List<Address>> {

			@Override
			protected List<Address> doInBackground(String... params) {
				// TODO Auto-generated method stub
				Geocoder geocoder = new Geocoder(getActivity());
				List<Address> addresses = null;

				// Getting a maximum of 3 Address that matches the input text
				try {
					addresses = geocoder.getFromLocationName(params[0], 3);

				} catch (Exception e) {
					// TODO: handle exception

					Toast.makeText(getActivity(), e.getMessage(), 3000).show();
				}

				return addresses;

			}

			@Override
			protected void onPostExecute(List<Address> results) {
				// TODO Auto-generated method stub

				if (results == null) {
					Toast.makeText(getActivity(), "No location", 3000).show();
				}

				//googleMap.clear();

				for (int i = 0; i < results.size(); i++) {

					Address address = (Address) results.get(i);

					LatLng latLng = new LatLng(address.getLatitude(),
							address.getLongitude());
					String addressText = String.format(
							"%s, %s",
							address.getMaxAddressLineIndex() > 0 ? address
									.getAddressLine(0) : "", address
									.getCountryName());
					
					Log.d("Searchaddress", addressText);

					etLocation.setText(addressText);
					
					marker = new MarkerOptions();
					marker.position(latLng);
					marker.title(addressText);
				}

			}
		}// end of async
	}// end o dialog class
}

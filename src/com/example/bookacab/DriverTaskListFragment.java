package com.example.bookacab;

import java.util.ArrayList;
import java.util.HashMap;

import com.example.bookacab.adapter.DriverListItemAdapter;
import com.example.bookacab.utils.AppConstants;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

public class DriverTaskListFragment extends Fragment {

	View rootView;
	ListView lv1;
	DriverListItemAdapter listDrAdapter;
	HashMap<String, String> map;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragmen_driver_task_list, container, false);

		ArrayList<HashMap<String, String>> driverList = new ArrayList<HashMap<String, String>>();
		map = new HashMap<String, String>();
		map.put(AppConstants.TAG_DRIVER_FROM, "Kathmandu");
		map.put(AppConstants.TAG_DRIVER_TO, "Lalitpur");
		map.put(AppConstants.TAG_DRIVER_IMAGE_URL, "http://www.manishm.com.np/manish.png");
		driverList.add(map);
		
		map = new HashMap<String, String>();
		map.put(AppConstants.TAG_DRIVER_FROM, "Ktms");
		map.put(AppConstants.TAG_DRIVER_TO, "Lalitpur");
		map.put(AppConstants.TAG_DRIVER_IMAGE_URL, "http://www.manishm.com.np/manish.png");
		driverList.add(map);
		map = new HashMap<String, String>();
		map.put(AppConstants.TAG_DRIVER_FROM, "Kathmandu");
		map.put(AppConstants.TAG_DRIVER_TO, "Lalitpur");
		map.put(AppConstants.TAG_DRIVER_IMAGE_URL, "http://www.manishm.com.np/manish.png");
		driverList.add(map);
	
		
		try {
			lv1 = (ListView) rootView.findViewById(R.id.listDriverTask);
			listDrAdapter = new DriverListItemAdapter(getActivity(),
					R.layout.list_item_select_vechile, driverList);
			lv1.setAdapter(listDrAdapter);
		} catch (Exception e) {
			Toast.makeText(getActivity(), e.getMessage(), 3000).show();
			// TODO: handle exception
		}
		
		

		return rootView;
	}

}

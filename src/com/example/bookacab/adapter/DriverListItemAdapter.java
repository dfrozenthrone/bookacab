package com.example.bookacab.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bookacab.R;
import com.example.bookacab.utils.ImageLoader;

public class DriverListItemAdapter extends ArrayAdapter<HashMap<String, String>> {
	
	
	private Activity activity;
	ArrayList<HashMap<String, String>> eventsList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;
	public ImageLoader imageLoader;

	// public ImageView imageview;
	// public TextView tName, tDate;

	public DriverListItemAdapter(Context context, int resource,
			ArrayList<HashMap<String, String>> objects) {
		super(context, resource, objects);
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Resource = resource;
		eventsList = objects;
		imageLoader = new ImageLoader(context);
		
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// convert view = design
		View v = convertView;
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			holder.imageview = (ImageView) v
					.findViewById(R.id.imgListDriver);
			holder.tvDriverFrom = (TextView) v.findViewById(R.id.tvListFromDriver);

			holder.tvDriverTo = (TextView) v.findViewById(R.id.tvListToDriver);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		//Log.d("ITEM NAME:::::::::", eventsList.get(position).get("auction_id"));

		holder.imageview.setImageResource(R.drawable.ic_car);
		
		imageLoader.DisplayImage(
				eventsList.get(position).get(com.example.bookacab.utils.AppConstants.TAG_DRIVER_IMAGE_URL),
				holder.imageview);
		
		holder.tvDriverFrom.setText(eventsList.get(position)
				.get(com.example.bookacab.utils.AppConstants.TAG_DRIVER_FROM).toString());
		
		holder.tvDriverTo.setText(eventsList.get(position)
				.get(com.example.bookacab.utils.AppConstants.TAG_DRIVER_TO).toString());
		
		
		
		return v;

	}

	static class ViewHolder {
		public ImageView imageview;
		public TextView tvDriverFrom;
		public TextView tvDriverTo;

	}

	
}

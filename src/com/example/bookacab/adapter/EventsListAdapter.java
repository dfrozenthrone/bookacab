package com.example.bookacab.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bookacab.R;
import com.example.bookacab.utils.AppConstants;
import com.example.bookacab.utils.ImageLoader;

public class EventsListAdapter extends ArrayAdapter<HashMap<String, String>> {
	private Activity activity;
	ArrayList<HashMap<String, String>> eventsList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;
	public ImageLoader imageLoader;

	// public ImageView imageview;
	// public TextView tName, tDate;

	public EventsListAdapter(Context context, int resource,
			ArrayList<HashMap<String, String>> objects) {
		super(context, resource, objects);
		// Log.d("WHERE M I3???????", "U R HERE!!!!!!!");
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Resource = resource;
		eventsList = objects;
		imageLoader = new ImageLoader(context);
		// Log.d("WHERE M I1???????", "U R HERE!!!!!!!");
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// convert view = design
		View v = convertView;
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			holder.imageview = (ImageView) v
					.findViewById(R.id.eventImg);
			holder.tId = (TextView) v.findViewById(R.id.eventId);

			holder.tTitle = (TextView) v.findViewById(R.id.eventTitleTxt);
			holder.tDate = (TextView) v.findViewById(R.id.eventDateTxt);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		//Log.d("ITEM NAME:::::::::", eventsList.get(position).get("auction_id"));

		holder.imageview.setImageResource(R.drawable.ic_launcher);
		imageLoader.DisplayImage(
				eventsList.get(position).get(AppConstants.TAG_PICTURE),
				holder.imageview);
		
		holder.tId.setText(eventsList.get(position)
				.get(AppConstants.TAG_PID).toString());
		
		holder.tTitle.setText(eventsList.get(position)
				.get(AppConstants.TAG_TITLE).toString());
		
		holder.tDate.setText(eventsList.get(position)
				.get(AppConstants.TAG_DATE).toString());
		
		return v;

	}

	static class ViewHolder {
		public ImageView imageview;
		public TextView tId;
		public TextView tTitle;
		public TextView tDate;

	}

	
}

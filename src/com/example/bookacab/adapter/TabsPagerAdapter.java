package com.example.bookacab.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.bookacab.DashboardActivity;
import com.example.bookacab.DriverTaskListFragment;
import com.example.bookacab.EventList;
import com.example.bookacab.GamesFragment;
import com.example.bookacab.GetaQuoteFragment;
import com.example.bookacab.LoginActivity;

public class TabsPagerAdapter extends FragmentPagerAdapter {

	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			// Top Rated fragment activity
			return new GetaQuoteFragment();
		case 1:
			// Games fragment activity
			return new DashboardActivity();
		
		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 2;
	}

}

package com.example.bookacab.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.bookacab.R;
import com.example.bookacab.utils.AppConstants;
import com.example.bookacab.utils.ImageLoader;

public class SelectVechileAdapter extends ArrayAdapter<HashMap<String, String>> {
	private Activity activity;
	ArrayList<HashMap<String, String>> eventsList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;
	public ImageLoader imageLoader;

	// public ImageView imageview;
	// public TextView tName, tDate;

	public SelectVechileAdapter(Context context, int resource,
			ArrayList<HashMap<String, String>> objects) {
		super(context, resource, objects);
		// Log.d("WHERE M I3???????", "U R HERE!!!!!!!");
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Resource = resource;
		eventsList = objects;
		imageLoader = new ImageLoader(context);
		// Log.d("WHERE M I1???????", "U R HERE!!!!!!!");
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// convert view = design
		View v = convertView;
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			holder.tvVEcName = (TextView) v.findViewById(R.id.tvListVecName);
			holder.tvVecDetail = (TextView) v
					.findViewById(R.id.tvListVecDetail);
			holder.imgVecIco = (ImageView) v.findViewById(R.id.imgListVecIco);
			holder.imgVecBookNow = (ImageView) v
					.findViewById(R.id.imgListVecBook);
			holder.rbVecOne = (RadioButton) v.findViewById(R.id.rbListOne);
			holder.rbVecTwo = (RadioButton) v.findViewById(R.id.rbListTwo);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		// Log.d("ITEM NAME:::::::::",
	
		holder.imgVecIco.setImageResource(R.drawable.ic_car);
		imageLoader.DisplayImage(
				eventsList.get(position).get(AppConstants.TAG_PICTURE),
				holder.imgVecIco);

		holder.tvVecDetail.setText(eventsList.get(position)
				.get(AppConstants.TAG_PID).toString());

		holder.tvVEcName.setText(eventsList.get(position)
				.get(AppConstants.TAG_TITLE).toString());

		return v;

	}

	static class ViewHolder {
		TextView tvVEcName, tvVecDetail;
		ImageView imgVecIco, imgVecBookNow;
		RadioButton rbVecOne, rbVecTwo;

	}

}

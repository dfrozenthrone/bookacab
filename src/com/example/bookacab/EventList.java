package com.example.bookacab;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookacab.adapter.EventsListAdapter;
import com.example.bookacab.utils.AppConstants;
import com.example.bookacab.utils.ConnectionDetector;
import com.example.bookacab.utils.JSONParser;

@SuppressLint("NewApi")
public class EventList extends Fragment {

	private ListView eventList;
	private JSONParser jParser = new JSONParser();
	private JSONArray products;
	private ArrayList<HashMap<String, String>> eventssList;
	private ProgressDialog pDialog;
	// private String eventId,eventTitle,eventDate,eventImage;
	private EventsListAdapter eventAdapter;
	// private ArrayAdapter<String> adapter;
	private TextView eventTxt;

	boolean flag = true;

	private String[] events = { "Summer bash part", "Winter dance  party" };

	// ArrayList<HashMap<String, String>> eventsLists;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragmen_driver_task_list,
				container, false);

		/*
		 * if (flag == true) { eventList = (ListView)
		 * rootView.findViewById(R.id.listDriverTask);
		 * 
		 * eventssList = new ArrayList<HashMap<String, String>>();
		 * 
		 * new LoadAllEvents().execute(); flag = false;
		 * 
		 * }
		 */
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);

		if (flag) {
			eventList = (ListView) view.findViewById(R.id.listDriverTask);

			eventssList = new ArrayList<HashMap<String, String>>();

			eventAdapter = new EventsListAdapter(getActivity(),
					R.layout.row_events, eventssList);

			eventList.setAdapter(eventAdapter);

			new LoadAllEvents().execute();
			flag=false;
		}

	}

	class LoadAllEvents extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			/*
			 * pDialog = ProgressDialog.show(getApplicationContext(), "",
			 * "Loading...", true);
			 */
		}

		// getting all product from url
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(
					AppConstants.url_all_products, "GET", params);

			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());

			try {
				// Checking for SUCCESS TAG
				int success = json.getInt(AppConstants.TAG_SUCCESS);

				if (success == 1) {
					// products found
					// Getting Array of Products
					products = json.getJSONArray(AppConstants.TAG_EVENTS);

					// looping through All Products
					for (int i = 0; i < products.length(); i++) {
						JSONObject c = products.getJSONObject(i);

						// Storing each json item in variable
						String eventId = c.getString(AppConstants.TAG_PID);
						String eventTitle = c.getString(AppConstants.TAG_TITLE);
						String eventDate = c.getString(AppConstants.TAG_DATE);
						String eventImage = c
								.getString(AppConstants.TAG_PICTURE);
						/*
						 * Log.d("json", eventId); Log.d("jsont", eventTitle);
						 * Log.d("jsond", eventDate);
						 * 
						 * Log.d("jsoni", eventImage); Integer len =
						 * products.length();
						 * 
						 * Log.d("lenght", len.toString());
						 */

						Log.d("imageurl", eventImage);

						// creating new HashMap
						HashMap<String, String> hashmap = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						hashmap.put(AppConstants.TAG_PID, eventId);
						hashmap.put(AppConstants.TAG_TITLE, eventTitle);
						hashmap.put(AppConstants.TAG_DATE, eventDate);
						hashmap.put(AppConstants.TAG_PICTURE, eventImage);

						// adding HashList to ArrayList
						eventssList.add(hashmap);

					}
				} else {
					Toast.makeText(getActivity(), "Error", 3000).show();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			// pDialog.dismiss();
			// updating UI from Background Thread
			/*
			 * runOnUiThread(new Runnable() {
			 * 
			 * public void run() { eventAdapter = new EventsListAdapter(
			 * getActivity(), R.layout.row_events, eventssList);
			 * 
			 * eventList.setAdapter(eventAdapter);
			 * 
			 * } });
			 */
			eventAdapter.notifyDataSetChanged();
		}

	}
}

package com.example.bookacab;

import android.app.Activity;
import android.os.Bundle;

public class UserSettings extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Display the fragment as the main content.
		getFragmentManager().beginTransaction()
				.replace(android.R.id.content, new UserFragment()).commit();
	}

}

package com.example.bookacab;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class GetaQuoteFragment extends Fragment {

	View rootView;
	Button btnGetQ, btnCallQ;
	String phone = "tel:9843677237";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_first_quote, container,
				false);

		btnCallQ = (Button) rootView.findViewById(R.id.btnGiveCallFirst);
		btnGetQ = (Button) rootView.findViewById(R.id.btnGetaQuoteFirst);

		btnGetQ.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				GetaQuoteFormFragment dFrag = new GetaQuoteFormFragment();
				android.support.v4.app.FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				// Replace whatever is in the fragment_container view with this
				// fragment,
				// and add the transaction to the back stack
				transaction.add(R.id.fragmentGetaQuoteFirst, dFrag);

				transaction.commit();
			}
		});

		btnCallQ.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(phone));
				startActivity(intent);
			}
		});

		return rootView;
	}
}

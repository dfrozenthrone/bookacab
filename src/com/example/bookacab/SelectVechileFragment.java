package com.example.bookacab;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

public class SelectVechileFragment extends Fragment {
	
	View rootView;
	TextView tvVEcName, tvVecDetail ;
	ImageView imgVecIco ,imgVecBookNow;
	RadioButton rbVecOne,rbVecTwo;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		 rootView  = inflater.inflate(R.layout.fragment_select_vechile, container, false);
		
		
		return rootView;
	}
	
	
	
	private void intializeComp()
	{
		tvVEcName		= (TextView) rootView.findViewById(R.id.tvListVecName);
		tvVecDetail		= (TextView) rootView.findViewById(R.id.tvListVecDetail);
		imgVecIco		=(ImageView) rootView.findViewById(R.id.imgListVecIco);
		imgVecBookNow		=(ImageView) rootView.findViewById(R.id.imgListVecBook);
		rbVecOne		= (RadioButton) rootView.findViewById(R.id.rbListOne);
		rbVecTwo		= (RadioButton) rootView.findViewById(R.id.rbListTwo);

		}
	
	

}

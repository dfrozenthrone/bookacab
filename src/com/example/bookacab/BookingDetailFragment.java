package com.example.bookacab;

import java.io.IOException;
import java.util.List;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class BookingDetailFragment extends Fragment implements
		OnMarkerDragListener {

	GoogleMap googleMap;
	private FragmentActivity mContext;
	private FragmentManager fManager;
	View rootView;

	private List<Address> addressList;
	MarkerOptions marker;
	private LatLng location;
	private double LatitudeD, LongitudeD;
	
	
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		rootView = inflater.inflate(R.layout.fragment_booking_details,
				container, false);

		try {
			googleMap = ((SupportMapFragment) getFragmentManager()
					.findFragmentById(R.id.mapBookDetails)).getMap();

			setupMap();

			googleMap.setOnMapClickListener(new OnMapClickListener() {

				@Override
				public void onMapClick(LatLng latLong) {
					// TODO Auto-generated method stub

					// Log.d("latitute",latLong.latitude + latLong.longitude);
				}
			});

		} catch (Exception e) {
			// TODO: handle exception
			Toast.makeText(getActivity(), e.getMessage(), 3000).show();
		}

		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

	}

	@Override
	public void onResume() {
		super.onResume();
		// initilizeMap();

	}

	private void setupMap() {
		googleMap.setMapType(googleMap.MAP_TYPE_NORMAL);
		// Showing / hiding your current location
		googleMap.setMyLocationEnabled(true);

		// Enable / Disable zooming controls
		googleMap.getUiSettings().setZoomControlsEnabled(true);

		// Enable / Disable my location button
		googleMap.getUiSettings().setMyLocationButtonEnabled(true);

		// Enable / Disable Compass icon
		googleMap.getUiSettings().setCompassEnabled(true);

		// Enable / Disable Rotate gesture
		googleMap.getUiSettings().setRotateGesturesEnabled(true);

		// Enable / Disable zooming functionality
		googleMap.getUiSettings().setZoomGesturesEnabled(true);

		// Adding a marker
		marker = new MarkerOptions().position(new LatLng(27.72517, 85.325861))
				.title("Hello Maps ");

		marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.cab_ico));

		marker.draggable(true);

		googleMap.addMarker(marker);

		CameraPosition cameraPosition = new CameraPosition.Builder()
				.target(new LatLng(27.72517, 85.325861)).zoom(15).build();

		googleMap.animateCamera(CameraUpdateFactory
				.newCameraPosition(cameraPosition));
	}

	private List<Address> getLocationFrom(double lat, double longitude,
			int maxResults) {
		Geocoder geoCoder = new Geocoder(getActivity());
		List<Address> address = null;
		try {
			address = geoCoder.getFromLocation(lat, longitude, maxResults);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Toast.makeText(getActivity(), e.getLocalizedMessage().toString(),
					Toast.LENGTH_LONG).show();
		}

		return address;
	}

	private double[] giveLatLong() {
		location = marker.getPosition();

		double[] locDoubleList = null;

		locDoubleList[0] = location.latitude;
		locDoubleList[1] = location.longitude;

		return locDoubleList;

	}

	@Override
	public void onMarkerDrag(Marker arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMarkerDragEnd(Marker arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMarkerDragStart(Marker arg0) {
		// TODO Auto-generated method stub

	}

	/*
	 * private void initilizeMap() { if (googleMap == null) { googleMap =
	 * ((SupportMapFragment)
	 * getFragmentManager().findFragmentById(R.id.mapBookDetails)).getMap();
	 * 
	 * // check if map is created successfully or not if (googleMap == null) {
	 * Toast.makeText(getActivity(), "Sorry! unable to create maps",
	 * Toast.LENGTH_SHORT).show(); } } }
	 */

}

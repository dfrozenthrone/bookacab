package com.example.bookacab;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.bookacab.utils.AlertDialogManager;
import com.example.bookacab.utils.SessionManager;

@SuppressLint("NewApi")
public class LoginActivity extends Activity {

	View rootView;
	EditText etUserName, etPass;
	Button btnSignin;
	RelativeLayout loginLayout;
	TextView tvForgotPass, tvCreateAcc;
	SessionManager session;
	AlertDialogManager alert = new AlertDialogManager();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.login_layout);
		
		initalizeComp();

		session = new SessionManager(LoginActivity.this);

		btnSignin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				String username = etUserName.getText().toString();
				String password = etPass.getText().toString();
				// Check if username, password is filled
				if (username.trim().length() > 0
						&& password.trim().length() > 0) {
					// For testing puspose username, password is checked with
					// sample data
					// username = test
					// password = test
					if (username.equals("test") && password.equals("test")) {

						// Creating user login session
						// For testing i am stroing name, email as follow
						// Use user real data
						session.createLoginSession("Android Hive",
								"anroidhive@gmail.com");

						// Staring MainActivity
						Intent i = new Intent(getApplicationContext(),
								MainActivity.class);
						startActivity(i);
						finish();

					} else {
						// username / password doesn't match
						alert.showAlertDialog(LoginActivity.this,
								"Login failed..",
								"Username/Password is incorrect", false);
					}
				} else {
					// user didn't entered username or password
					// Show alert asking him to enter the details
					alert.showAlertDialog(LoginActivity.this, "Login failed..",
							"Please enter username and passwords", false);
				}

			}
		});

		tvCreateAcc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(LoginActivity.this,
						RegisterActivity.class);
				startActivity(intent);
			}
		});
	}

	private void initalizeComp() {
		etUserName = (EditText) findViewById(R.id.etEmail);
		etPass = (EditText) findViewById(R.id.etPass);
		btnSignin = (Button) findViewById(R.id.btnLogin);
		loginLayout = (RelativeLayout) findViewById(R.id.fragmentLogin);
		tvCreateAcc = (TextView) findViewById(R.id.tvLoginRegister);
		tvForgotPass = (TextView) findViewById(R.id.tvLoginForgotPass);

	}

}
